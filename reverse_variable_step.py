import rounding
import task

def rev_var_step(left, right, accuracy):
    step = (right - left)/5
    x_min = left + step
    while abs(step) > accuracy:
        x = x_min + step
        func_x = task.function(x)
        func_min = task.function(x_min)
        if func_x < func_min:
            x_min = x
            func_min = func_x
        else:
            step = -1*step/3
    raw_x_result = x_min
    raw_y_result = task.function(raw_x_result)
    rounded_x_result = rounding.rounding(raw_x_result, accuracy)
    rounded_y_result = rounding.rounding(raw_y_result, accuracy)
    return rounded_x_result, rounded_y_result

answ = rev_var_step(task.x1, task.x4, task.e)

print(answ)
