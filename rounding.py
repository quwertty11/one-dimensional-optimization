import task

def rounding(raw_result, accuracy):
    int_result = int(round(raw_result/accuracy))
    rounded_result = int_result*accuracy
    del raw_result, int_result
    return rounded_result
