import matplotlib.pyplot as plt
import task

x = []
for i in range(-50,50,1):
    x.append(i/10)
y = []
for i in range(-50,50,1):
    y.append(task.function(i/10))

plt.plot(x, y)
plt.grid(color='r', linestyle='-', linewidth=1, alpha=0.5)
plt.show()
