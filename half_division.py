import rounding
import task

def half_division(left, right, accuracy):
    while abs(right - left) > 2*accuracy:
        center_left = (right + left)/2
        center_right = center_left + accuracy/3
        if task.function(center_left) > task.function(center_right):
            left = center_left
        else:
            right = center_right
    raw_x_result = (left+right)/2
    raw_y_result = task.function(raw_x_result)
    rounded_x_result = rounding.rounding(raw_x_result, accuracy)
    rounded_y_result = rounding.rounding(raw_y_result, accuracy)
    return rounded_x_result, rounded_y_result

answ = half_division(task.x1, task.x4, task.e)

print(answ)
