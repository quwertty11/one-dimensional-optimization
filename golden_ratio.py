import math
import rounding
import task

def golden_ratio(left, right, accuracy):
    GoldenRatio = (3 - math.sqrt(5))/2
    center_left = left + GoldenRatio*(right - left)
    center_right = right - GoldenRatio*(right - left)
    func_c_left = task.function(center_left)
    func_c_right = task.function(center_right)
    while abs(right - left) > 2*accuracy:
        if func_c_right > func_c_left:
            right = center_right
            center_right = center_left
            func_c_right = func_c_left
            center_left = left + GoldenRatio*(right -left)
            func_c_left = task.function(center_left)
        else:
            left = center_left
            center_left = center_right
            func_c_left = func_c_right
            center_right = right - GoldenRatio*(right - left)
            func_c_right = task.function(center_right)
    raw_x_result = (left+right)/2
    raw_y_result = task.function(raw_x_result)
    rounded_x_result = rounding.rounding(raw_x_result, accuracy)
    rounded_y_result = rounding.rounding(raw_y_result, accuracy)
    return rounded_x_result, rounded_y_result

answ = golden_ratio(task.x1, task.x4, task.e)

print(answ)
