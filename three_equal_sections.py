import rounding
import task

def three_eq_sect(left, right, accuracy):
    while abs(right - left) > 2*accuracy:
        center_left = left + (right-left)/3
        center_right = right - (right-left)/3
        if task.function(center_left) > task.function(center_right):
            left = center_left
        else:
            right = center_right
    raw_x_result = (left+right)/2
    raw_y_result = task.function(raw_x_result)
    rounded_x_result = rounding.rounding(raw_x_result, accuracy)
    rounded_y_result = rounding.rounding(raw_y_result, accuracy)
    return rounded_x_result, rounded_y_result

answ = three_eq_sect(task.x1, task.x4, task.e)

print(answ)
